<!DOCTYPE html>
<html lang="en">
<head>
<title>Kelompok 3</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial, Helvetica, sans-serif;
}

/* Style  header */
header {
  background-color: black ;
  padding: 20px;
  text-align: center;
  font-size: 40px;
  color: #FF0000;
}

/* bikin 2 kolom, satu left, satu right-menu */
nav {
  float: right;
  width: 20%;
  font-size: 40px;
  height: 300px; /*mbuh artine apa */
  background: #FF6347;
  padding: 0px;
}

/* Style the list inside the menu */
nav ul {
  list-style-type: none;
  padding: 20px; /* tepi isi dari tulisan*/
}

article {
  float: left;
  padding: 20px;
  width: 80%;
  background-color: #D8BFD8 ;
  height: 300px; /* only for demonstration, should be removed */
}

/* Clear floats after the columns */
section:after {
  content: "";
  display: table;
  clear: both;
}

/* Style the footer */
footer {
  background-color: #006400 ;
  font-size: 20px;
  padding: 10px;
  text-align: center;
  color: white;
}

/* Responsive layout - responsive bro, on small screens */
@media (max-width: 1360px) {
  nav, article {
    width: 100%;
    height: auto;
  }
}
</style>
</head>
<body>


<header>
  <h2>KESEHATAN PRIA</h2>
</header>

<section>
  
  <nav>
    <ul>
      <li><a href="#">Kuliner</a></li>
      <li><a href="#">Teknologi</a></li>
      <li><a href="#">Sport</a></li>
    </ul>
  </nav>
  
  <article>
  <img>
    <h1>Lontong</h1>
    <p>London is the capital city of England. It is the most populous city in the  United Kingdom, with a metropolitan area of over 13 million inhabitants.</p>
    <p>Standing on the River Thames, London has been a major settlement for two millennia, its history going back to its founding by the Romans, who named it Londinium.</p>
  </article>
</section>

<footer>
<img src="images/man.JPG">
  <p>DIKLAT PRANATA KOMPUTER TINGKAT AHLI 2019<BR>
  KELOMPOK 3</p>
</footer>

</body>
</html>
